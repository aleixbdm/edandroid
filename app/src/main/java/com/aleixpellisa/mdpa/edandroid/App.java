package com.aleixpellisa.mdpa.edandroid;

import android.app.Application;

public class App extends Application {

    private static App mInstance;

    public static synchronized App getInstance() {
        return mInstance;
    }

    private Repository repository;

    public Repository getRepository(){
        return repository;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        repository = new Repository();

    }
}
