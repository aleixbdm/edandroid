package com.aleixpellisa.mdpa.edandroid;

class Constants {

    /**
     * A dummy authentication store containing known user names and passwords.
     */
    static final String[] DUMMY_CREDENTIALS = new String[]{
            "master:master", "mdpa:master",
            "master:mdpa", "mdpa:mdpa"
    };

    static final String[] DUMMY_USERNAMES = new String[]{
            "master", "mdpa"
    };

    static String USERNAME_END = "@mail.com";

    static String[] USER_CARD_IMAGE_LINKS = new String[]{
            "https://source.unsplash.com/Xq1ntWruZQI/600x800",
            "https://source.unsplash.com/NYyCqdBOKwc/600x800",
            "https://source.unsplash.com/buF62ewDLcQ/600x800",
            "https://source.unsplash.com/THozNzxEP3g/600x800",
            "https://source.unsplash.com/USrZRcRS2Lw/600x800",
            "https://source.unsplash.com/PeFk7fzxTdk/600x800",
            "https://source.unsplash.com/LrMWHKqilUw/600x800",
            "https://source.unsplash.com/HN-5Z6AmxrM/600x800",
            "https://source.unsplash.com/CdVAUADdqEc/600x800",
            "https://source.unsplash.com/AWh9C-QjhE4/600x800"
    };

    static int USER_CARD_IMAGES_LOADED = 10;
    // To make "infinite" loops
    static int LOOPS_COUNT = 1000;

}
