package com.aleixpellisa.mdpa.edandroid;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class UserCardAdapter  extends FragmentStatePagerAdapter {

    private List<UserCard> userCardList;

    UserCardAdapter(FragmentManager fm) {
        super(fm);
        userCardList = App.getInstance().getRepository().getUserCards();
    }

    @Override
    public Fragment getItem(int position) {
        if (userCardList != null && userCardList.size() > 0) {
            position = position % userCardList.size(); // use modulo for infinite cycling
            return UserCardFragment.newInstance(userCardList.get(position));
        } else {
            return UserCardFragment.newInstance(null);
        }
    }

    @Override
    public int getCount() {
        if (userCardList != null && userCardList.size() > 0){
            return userCardList.size()*Constants.LOOPS_COUNT; // simulate infinite by big number of products
        } else {
            return 1;
        }
    }
}
