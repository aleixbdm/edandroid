package com.aleixpellisa.mdpa.edandroid;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class PhotosActivity extends AppCompatActivity {

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        viewPager = findViewById(R.id.photos_pager);
        viewPager.setAdapter(new UserCardAdapter(getSupportFragmentManager()));
        //Start at middle
        viewPager.setCurrentItem(Constants.LOOPS_COUNT/2);

        findViewById(R.id.photos_left_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movePrevious();
            }
        });

        findViewById(R.id.photos_right_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveNext();
            }
        });
    }

    public void movePrevious() {
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
    }

    public void moveNext() {
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }
}
