package com.aleixpellisa.mdpa.edandroid;

import android.content.Context;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

class Repository {

    private String username;
    private List<UserCard> userCards = new ArrayList<>();

    Repository() {
        initUserCards();
    }

    private Context getAppInstance() {
        return App.getInstance();
    }

    private void initUserCards() {
        for (String url : Constants.USER_CARD_IMAGE_LINKS) {
            userCards.add(new UserCard(url));

            // Preload image
            if (userCards.size() - 1 < Constants.USER_CARD_IMAGES_LOADED) {
                Glide.with(getAppInstance())
                        .load(url)
                        .submit();
            }
        }
    }

    // Getters & Setters

    String getUsername() {
        return username;
    }

    void setUsername(String username) {
        this.username = username;
    }

    List<UserCard> getUserCards() {
        return userCards;
    }
}
