package com.aleixpellisa.mdpa.edandroid;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class UserCardFragment extends Fragment {

    private UserCard userCard;

    public static UserCardFragment newInstance(UserCard userCard) {
        UserCardFragment userCardFragment = new UserCardFragment();
        userCardFragment.setArguments(userCard);
        return userCardFragment;
    }

    public void setArguments(UserCard userCard) {
        this.userCard = userCard;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.item_user_card, container, false);

        ImageView imageView = rootView.findViewById(R.id.item_user_card_image);

        if (userCard != null) {
            Glide.with(App.getInstance()).load(userCard.url).into(imageView);
        }

        return rootView;
    }
}

