package com.aleixpellisa.mdpa.edandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView username = findViewById(R.id.main_username);
        String usernameValue = App.getInstance().getRepository().getUsername() + Constants.USERNAME_END;
        username.setText(usernameValue);

        findViewById(R.id.main_show_photos_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPhotosActivity();
            }
        });
    }

    private void startPhotosActivity(){
        Intent intent = new Intent(MainActivity.this, PhotosActivity.class);
        startActivity(intent);
    }
}
